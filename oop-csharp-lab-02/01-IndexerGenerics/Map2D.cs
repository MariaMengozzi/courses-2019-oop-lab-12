﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;
        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach(TKey1 k1 in keys1)
            {
                foreach(TKey2 k2 in keys2 )
                {
                    values.Add(new Tuple<TKey1, TKey2>(k1,k2), generator.Invoke(k1, k2));
                }
            }
            
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            throw new NotImplementedException();
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                return values[new Tuple<TKey1, TKey2>(key1,key2)];
            }

            set
            {
                values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> list = new List<Tuple<TKey2, TValue>>();
            foreach(Tuple<TKey1,TKey2> k in values.Keys)
            {
                if (k.Item1.Equals(key1))
                {
                    list.Add(new Tuple<TKey2, TValue>(k.Item2, this[key1,k.Item2]));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> list = new List<Tuple<TKey1, TValue>>();
            foreach (Tuple<TKey1, TKey2> k in values.Keys)
            {
                if (k.Item2.Equals(key2))
                {
                    list.Add(new Tuple<TKey1, TValue>(k.Item1, this[k.Item1, key2]));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> list = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> k in values.Keys)
            {
                list.Add(new Tuple<TKey1, TKey2, TValue>(k.Item1, k.Item2, this[k.Item1, k.Item2]));
            }
            return list;
        }

        public int NumberOfElements
        {
            get
            {
                return values.Count;
            }
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, values);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
