using System;
using System.Collections.Generic;
using System.Linq;

namespace Iterators {

    public static class Java8StreamOperations
    {
        public static void ForEach<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            foreach (var t in sequence)
            {
                consumer.Invoke(t);
            }
        }

        public static IEnumerable<TAny> Peek<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            foreach (var t in sequence)
            {
                consumer.Invoke(t);
            }
            return sequence;
        }

        public static IEnumerable<TOther> Map<TAny, TOther>(this IEnumerable<TAny> sequence, Func<TAny, TOther> mapper)
        {
            IList<TOther> res = new List<TOther>();
            foreach (var x in sequence)
            {
                res.Add(mapper.Invoke(x));
            }
            return res;
        }

        public static IEnumerable<TAny> Filter<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            IList<TAny> res = new List<TAny>();
            foreach (var x in sequence)
            {
                if (consumer.Invoke(x))
                {
                    res.Add(x);
                }
            }
            return res;
        }

        public static IEnumerable<Tuple<int, TAny>> Indexed<TAny>(this IEnumerable<TAny> sequence)
        {
            IList<Tuple<int, TAny>> res = new List<Tuple<int, TAny>>();
            int count = 0;
            foreach(var t in sequence)
            {
                res.Add(new Tuple<int, TAny>(count, t));
                count++;
            }
            return res;
        }

        public static TOther Reduce<TAny, TOther>(this IEnumerable<TAny> sequence, TOther seed, Func<TOther, TAny, TOther> reducer)
        {
            TOther res = seed;
            foreach(var t in sequence)
            {
                res = reducer.Invoke(res, t);
            }
            return res;
        }

        public static IEnumerable<TAny> SkipWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            IList<TAny> res = new List<TAny>();
            IEnumerator<TAny> iterator = sequence.GetEnumerator();
            while (iterator.MoveNext() && consumer(iterator.Current))
            { }
            while (iterator.Current != null)
            {
                res.Add(iterator.Current);
                iterator.MoveNext();
            }
            return res;
        }

        public static IEnumerable<TAny> SkipSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            IList<TAny> res = new List<TAny>();
            IEnumerator<TAny> iterator = sequence.GetEnumerator();
            iterator.MoveNext();
            for (; count > 0; count--)
            {
                iterator.MoveNext();
            }
            do
            {
                res.Add(iterator.Current);
            } while (iterator.MoveNext());
            return res;
        }

        public static IEnumerable<TAny> TakeWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            IList<TAny> res = new List<TAny>();
            foreach (var x in sequence)
            {
                if (consumer.Invoke(x))
                {
                    res.Add(x);
                }
                else
                {
                    break;
                }
            }
            return res;
        }

        public static IEnumerable<TAny> TakeSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            IList<TAny> res = new List<TAny>();
            foreach (var x in sequence)
            {
                if (count > 0)
                {
                    res.Add(x);
                    count--;
                }
                else
                {
                    break;
                }
            }

            return res;
        }

        public static IEnumerable<int> Integers(int start)
        {
            return Enumerable.Range(start,int.MaxValue);
        }

        public static IEnumerable<int> Integers()
        {
            return Integers(0);
        }

        public static IEnumerable<int> Range(int start, int count)
        {
            return Integers(start).TakeSome(count);
        }
    }

}