﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>   
    {
        private List<TItem> list;
        public ObservableList()
        {
            list = new List<TItem>();
        }

        public IEnumerator<TItem> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            list.Add(item);
            if (this.ElementInserted != null)
            {
                this.ElementInserted.Invoke(this, item, list.Count-1);
            }
        }

        public void Clear()
        {
            list.Clear(); ;
        }

        public bool Contains(TItem item)
        {
            return list.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            
            if (this.ElementRemoved != null)
            {
                this.ElementRemoved.Invoke(this, item, IndexOf(item));
            }
            return list.Remove(item);
        }

        public int Count
        {
            get;
        }

        public bool IsReadOnly
        {
            get;
        }

        public int IndexOf(TItem item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            list.Insert(index, item);
            if (this.ElementInserted != null)
            {
                this.ElementInserted.Invoke(this, item, list.Count-1);
            }
        }

        public void RemoveAt(int index)
        {
            
            if (this.ElementRemoved != null)
            {
                this.ElementRemoved.Invoke(this, this[index], index);
            }
            list.RemoveAt(index);
        }

        public TItem this[int index]
        {
            get { return this[index]; }
            set
            {
                TItem oldvalue = list[index];
                list[index] = value;
                if (this.ElementChanged != null)
                {
                    this.ElementChanged.Invoke(this, value, oldvalue, index);
                }
            }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}